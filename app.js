const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express')
const dotenv = require('dotenv');
// const cors = require('cors');
const bodyParser = require('body-parser');
const port_http = process.env.PORT || 8440;
const port_https = process.env.PORT || 8443;

const credentials = {
    key: fs.readFileSync('/etc/letsencrypt/live/ticketing.selindo.com/privkey.pem', 'utf8'),
    cert: fs.readFileSync('/etc/letsencrypt/live/ticketing.selindo.com/cert.pem', 'utf8')
}

dotenv.config();
express.application.prefix = express.Router.prefix = function (path, configure) {
    const router = express.Router();
    this.use(path, router);
    configure(router);
    return router;
}

const app = express();
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

//?parse application/json
app.use(bodyParser.urlencoded({ extended: true, limit: '25mb' }));
app.use(bodyParser.json({ limit: '25mb' }));
// app.use(cors());

//? routes api endpoint
const routes = require('./routes');
routes(app);

httpServer.listen(port_http, () => console.log(`http://localhost:${port_http}`));
httpsServer.listen(port_https, () => console.log(`https://localhost:${port_https}`));

