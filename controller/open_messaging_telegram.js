const axios = require('axios');
const platformClient = require('purecloud-platform-client-v2');
const { access_token } = require('./access_token');
const { logger, response } = require('../helper');

exports.inbound_telegram = async (req, res) => {
    if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
    const data = req.body;
    const token = await access_token();
    logger('access_token', token);

    const client = platformClient.ApiClient.instance;
    client.setEnvironment(platformClient.PureCloudRegionHosts.ap_southeast_2);
    client.setAccessToken(token.accessToken);

    const apiInstance = new platformClient.ConversationsApi();
    let body = {
        id: data.update_id,
        channel: {
            platform: "Open",
            type: "Private",
            messageId: data.message.from.id,
            to: {
                id: process.env.CHANNEL_ID //channel integration id
            },
            from: {
                nickname: data.message.from.username,
                id: data.message.from.id,
                idType: "Opaque",
                image: '',
                firstName: data.message.from.first_name,
                lastName: ''
            },
            time: new Date()
        },
        type: "Text",
        text: data.message.text,
        direction: "Inbound"
    }

    // Send an inbound Open Message
    apiInstance.postConversationsMessagesInboundOpen(body)
        .then((result) => {
            logger('inbound_telegram', result);
            response.ok(res, result);
        })
        .catch((error) => {
            response.error(res, error.message, 'ERROR/inbound_telegram');
        });
}

exports.outbound_telegram = async (req, res) => {
    if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
    const data = req.body;

    try {
        const BOT_TOKEN = process.env.TELEGRAM_TOKEN;
        const BASE_URL = `https://api.telegram.org/bot${BOT_TOKEN}`;
        let config = {
            url: `${BASE_URL}/sendMessage`,
            method: 'post',
            maxBodyLength: Infinity,
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify({
                chat_id: data.channel.to.id,
                text: data.text
            })
        }
        logger('telegram-bot/outbound_telegram', config);

        await axios(config)
            .then(function (result) {
                logger('telegram-bot/outbound_telegram', result.data);
                response.ok(res, result.data);
            })
            .catch(function (error) {
                response.error(res, error.message, 'ERROR/telegram-bot/outbound_telegram');
            });
    }
    catch (error) {
        response.error(res, error, 'ERROR/telegram-bot/outbound_telegram');
    }
}