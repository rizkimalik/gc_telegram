'use strict';

const messaging = require("./controller/open_messaging_telegram");

module.exports = function (app) {
    app.route('/').get((req, res) => {
        res.json({ message: "Application API running! 🤘🚀" });
        res.end();
    });

    app.prefix('/messaging', function (api) {
        api.route('/inbound_telegram').post(messaging.inbound_telegram);
        api.route('/outbound_telegram').post(messaging.outbound_telegram);
    });
}